﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyMove : MonoBehaviour {

	private GameTimeCounter gtc;

	private float Horizontal;
	private float Vertical;
	private float x;
	private float y;

	void Start () {
		gtc = GameObject.Find ("GameManager").GetComponent<GameTimeCounter> ();
		Horizontal = Random.Range(-1f,1f);
		Vertical = Random.Range(-1f,1f);
	}
	
	void Update(){
		if (gtc.isHideTime) {
			this.move ();
		}
	}

	void OnCollisionEnter2D (Collision2D col)
	{
		if (col.gameObject.tag == "ColliderSide") {
			Horizontal = Horizontal * -1;
		}
		if (col.gameObject.tag == "ColliderUpDown") {
			Vertical = Vertical * -1;
		}
	}


	void move() {
		x += Horizontal;
		y += Vertical;

		this.transform.localPosition =  new Vector2(x,y);
	}

}
