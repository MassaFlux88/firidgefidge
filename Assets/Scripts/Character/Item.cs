﻿using UnityEngine;
using System.Collections;

public class Item : MonoBehaviour {

	public enum ItemKind{
		soap,
		softener,
		Cloth1,
		Cloth2,
		Cloth3,
		Cloth4
	}
	public ItemKind item;

	private GameObject Camera;

	void OnTriggerEnter2D (Collider2D col) {
		if (col.gameObject.tag == "Player") {
			PlayerControl playerCtrl = col.GetComponent<PlayerControl> ();
			playerCtrl.GetItem(item);
			Destroy(gameObject);
		}
		if (col.gameObject.tag == "Collider") {
			Destroy(gameObject);
		}
	}

	void Start () {
		Camera = GameObject.Find("Main Camera");
	}

	void Update () {
		if(Camera.transform.position.x - this.transform.position.x >= 14f){
			Destroy(this.gameObject);
		}
	}
}
