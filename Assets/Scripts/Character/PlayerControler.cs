﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class PlayerControler : MonoBehaviour {

	[SerializeField]
	private float speed = 10.0f;

	private GameTimeCounter gtc;

	// Use this for initialization
	void Start () {
		gtc = GameObject.Find ("GameManager").GetComponent<GameTimeCounter> ();
	}

	void Update(){
		if (gtc.isHideTime) {
			this.move ();
		}
	}
	
	void move() {
		var direction = Vector3.zero;
		if (Input.GetKey (KeyCode.A)) {
			direction += Vector3.left;
		}
		if (Input.GetKey (KeyCode.D)) {
			direction += Vector3.right;
		}
		if (Input.GetKey (KeyCode.W)) {
			direction += Vector3.up;
		}
		if (Input.GetKey (KeyCode.S)) {
			direction += Vector3.down;
		}
		this.transform.localPosition += direction / speed;
	}

}
