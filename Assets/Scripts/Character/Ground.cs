﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ground : MonoBehaviour {

	private GameObject Camera;

	void Start () {
		Camera = GameObject.Find("Main Camera");
	}
	
	void Update () {
		if(Camera.transform.position.x - this.transform.position.x >= 25f){
			Destroy(this.gameObject);
		}
	}

}