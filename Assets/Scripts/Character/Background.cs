﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour {

	private GameObject Camera;
	private float size;

	void Start () {
		Camera = GameObject.Find("Main Camera");
		size = GetComponent<SpriteRenderer> ().bounds.size.x;
	}

	void Update () {
		if(Camera.transform.position.x - this.transform.position.x >= size){
			Destroy(this.gameObject);
		}
	}
}
