﻿using UnityEngine;
using System.Collections;
using UniRx;
using UniRx.Triggers;
using UnityEngine.UI;

public class PlayerControl : MonoBehaviour {

	private Rigidbody2D rb2D;

	public Button Boost;
	public Button Right;
	public Button Left;

	public ParticleSystem BubbleJet;
	public ParticleSystem DamageSpark;

	private MapMaker Mapmake;

	public AudioClip BubbleSound;
	AudioSource audioSource;

	Vector3 StartPosition;
	Vector3 RespaunPosition;
	Vector3 RestartPosition;

	public float speed = 5.0f;
	public int Score = 0;
	public float Dist;
	public float maxFuel;
	public float Fuel;
	public float maxHP;
	public float HP;
	private bool isHit;
	private bool isBoosted;
	private bool isRight;
	private bool isLeft;
	private bool isCharge;
	private bool isGameOver;

	void Start()
	{
		audioSource = gameObject.GetComponent<AudioSource> ();
		audioSource.clip = BubbleSound;

		rb2D = GetComponent<Rigidbody2D> ();

		Boost = GameObject.Find ("StatusCanvas/Boost").GetComponent<Button> ();
		Right = GameObject.Find ("StatusCanvas/Right").GetComponent<Button> ();
		Left = GameObject.Find ("StatusCanvas/Left").GetComponent<Button> ();
		BubbleJet = GameObject.Find ("BubbleJet").GetComponent<ParticleSystem> ();
		DamageSpark = GameObject.Find ("Spark").GetComponent<ParticleSystem> ();

		Mapmake = GameObject.Find("MAPMAKER").GetComponent<MapMaker>();

		BubbleJet.Stop ();
		DamageSpark.Stop ();

		Dist = 0;
		maxFuel = 1500f;
		maxHP = 50f;

		Fuel = maxFuel;
		HP = maxHP;

		var etBoost = Boost.gameObject.AddComponent<ObservableEventTrigger>();
		etBoost.OnPointerDownAsObservable ()
			.Subscribe (_ => 
				{
					audioSource.Play();
					BubbleJet.Play();
					isBoosted = true;
				})
			.AddTo (this);
		etBoost.OnPointerUpAsObservable ()
			.Subscribe (_ => 
				{
					BubbleJet.Stop();
					isBoosted = false;
				})
			.AddTo (this);

		var etRight = Right.gameObject.AddComponent<ObservableEventTrigger>();	
		etRight.OnPointerDownAsObservable ()
			.Subscribe (_ => 
				{
					audioSource.Play();
					BubbleJet.Play();
					isRight = true;
				})
			.AddTo (this);
		etRight.OnPointerUpAsObservable ()
			.Subscribe (_ => 
				{
					BubbleJet.Stop();
					isRight = false;
				})
			.AddTo (this);

		var etLeft = Left.gameObject.AddComponent<ObservableEventTrigger>();
		etLeft.OnPointerDownAsObservable ()
			.Subscribe (_ => 
				{
					audioSource.Play();
					BubbleJet.Play();
					isLeft = true;
				})
			.AddTo (this);
		etLeft.OnPointerUpAsObservable ()
			.Subscribe (_ => 
				{
					BubbleJet.Stop();
					isLeft = false;
				})
			.AddTo (this);
	}

	void Update()
	{
		Dist = this.gameObject.transform.position.x;
		if (Time.timeScale > 0) {
			fuelManager ();
			HPManager ();
		}
	}

	void FixedUpdate(){
		if (Fuel > 0) {
			this.move ();
		}
	}

	void OnCollisionEnter2D (Collision2D col)
	{
		if (col.gameObject.tag == "Collider") {
			isHit = true;
			SEPlayer.Instance.SparkSEPlayer ();
			DamageSpark.Play ();
		}
		if (col.gameObject.tag == "ChargeCollider") {
			isCharge = true;
			if (Fuel < maxFuel) {
				SEPlayer.Instance.ItemSEPlayer (4);
				SEPlayer.Instance.ItemSEPlayer (2);
			}
		}
	}

	void OnCollisionExit2D (Collision2D col)
	{
		if (col.gameObject.tag == "Collider") {
			isHit = false;
			DamageSpark.Stop ();
		}
	}

	void move() {
		float x = 0f;
		float y = 0f;
		float z = 0f;
		if (isRight || Input.GetKey (KeyCode.RightArrow)) {
			x = speed;
			z = -20;
		} 
		if (isLeft || Input.GetKey(KeyCode.LeftArrow))
		{
			x = -speed;
			z = 20;
		}
		if (isBoosted || Input.GetKey (KeyCode.Space)) {
			y = speed * 5f;
		}
		rb2D.AddForce(new Vector2(x , y));
		rb2D.rotation = z;
		return;
	}

	public void GetItem(Item.ItemKind itemKind)
	{
		switch (itemKind) {

		case Item.ItemKind.soap:
			if (Fuel < maxFuel)
			{
				Fuel = maxFuel;
				Score += 100;
			}
			SEPlayer.Instance.ItemSEPlayer(1);
			break;

		case Item.ItemKind.softener:
			HP += 40f;
			if (HP > maxHP)
			{
				HP = maxHP;
			}
			SEPlayer.Instance.ItemSEPlayer(3);
			break;

		case Item.ItemKind.Cloth1:
			Score += 100;
			SEPlayer.Instance.ItemSEPlayer(0);
			break;

		case Item.ItemKind.Cloth2:
			Score += 400;
			SEPlayer.Instance.ItemSEPlayer(0);
			break;
		}
	}
		

	void Restart()
	{
		transform.position = new Vector3(0,5,0);
	}

	void HPManager()
	{
		if (isHit)
		{
			if (HP >= 0)
			{
				HP -= 1.0f;
				if (HP == 0)
				{
					GameOver.Instance.GameOverMessage (Score, Dist);
				}
			}
		}
	}

	void fuelManager()
	{
		if ((isBoosted || isRight || isLeft) && Fuel > 0f)
		{
			Fuel -= 1.0f;
		}

		if (!isBoosted &&!isRight && !isLeft)
		{
			audioSource.Stop();
		}

		if (isCharge)
		{
			Fuel = maxFuel;
			isCharge = false;
			Mapmake.isSetBase = true;
		}
	}
}