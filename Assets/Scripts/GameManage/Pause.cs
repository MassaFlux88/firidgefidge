﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UniRx;

public class Pause : SingletonMonoBehaviour<FadeManager>{

	public Transform pauseCanvas;
	public Transform quitCanvas;
	public Button PauseButton;
	public Button Cancel;
	public Button Retry, Setting, Title;
	public Button bYes, bNo, quitCancel;
	private bool pauseGame = false;

	public PlayerControl pctrl;

	void Start() {

		pctrl = GameObject.Find("MAPMAKER").GetComponent<MapMaker> ().WashingMachine.GetComponent<PlayerControl>();

		OnUnPause();
		OnUnQuitCanvas ();

		Cancel.OnClickAsObservable ()
			.Subscribe (_ => {
				SEPlayer.Instance.ButtonSEPlayer(0);
				pauseGame = false;
				this.OnUnPause();
			}).AddTo (this);

		Retry.OnClickAsObservable ()
			.Take (1)
			.Subscribe (_ => {
				SceneChange.Instance.OnRetry();
				this.OnUnPause();
			}).AddTo (this);

		Setting.OnClickAsObservable ()
			.Subscribe (_ => {
				SEPlayer.Instance.ButtonSEPlayer(1);
			}).AddTo (this);
		
		Title.OnClickAsObservable ()
			.Subscribe (_ => {
				SceneChange.Instance.OnBriefing();
				this.OnQuitCanvas();
			}).AddTo (this);

		PauseButton.OnClickAsObservable ()
			.Subscribe (_ => {
				SEPlayer.Instance.ButtonSEPlayer(0);
				pauseGame = !pauseGame;
			}).AddTo (this);

		bYes.OnClickAsObservable ()
			.Subscribe (_ => {
				this.OnUnQuitCanvas();
				this.OnUnPause();
			}).AddTo (this);
		
		bNo.OnClickAsObservable ()
			.Subscribe (_ => {
				SEPlayer.Instance.ButtonSEPlayer(1);
				this.OnUnQuitCanvas();
			}).AddTo (this);

		quitCancel.OnClickAsObservable ()
			.Subscribe (_ => {
				SEPlayer.Instance.ButtonSEPlayer(1);
				this.OnUnQuitCanvas();
			}).AddTo (this);
		
	}

	public void Update() {

		if (Input.GetKeyDown (KeyCode.Escape)) {
			pauseGame = !pauseGame;
			SEPlayer.Instance.ButtonSEPlayer(0);
		}

		if (pauseGame == true){
			OnPause();
		}
		else{
			OnUnPause();
		}
	}

	public void OnPause() {
		pauseCanvas.gameObject.SetActive(true); 
		Time.timeScale = 0;
		pauseGame = true;

		pctrl.enabled = false;

	}

	public void OnUnPause() {
		pauseCanvas.gameObject.SetActive(false);
		Time.timeScale = 1;
		pauseGame = false;

		pctrl.enabled = true;

	}

	public void OnQuitCanvas(){
		quitCanvas.gameObject.SetActive(true); 
	}

	public void OnUnQuitCanvas(){
		quitCanvas.gameObject.SetActive(false);
	}

	public void OnRetry() {
		FadeManager.Instance.LoadLevel("Game",0.25f);
		return;
	}

	public void OnTitle () {
		FadeManager.Instance.LoadLevel("Briefing",0.25f);
		return;
	}

	public void OnResume() {
		OnUnPause();
		return;
	}
}