﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseClick : MonoBehaviour {

	private GameTimeCounter gtc;
	public GameObject Player;


	public bool isFind;
	public int ClickLife = 5;

	void Start () {
		isFind = false;
		gtc = GameObject.Find ("GameManager").GetComponent<GameTimeCounter> ();
	}
		
	// Update is called once per frame
	void Update () {
		if (gtc.isFindTime) {
			if (Input.GetMouseButtonDown (0)) {
				SEPlayer.Instance.TypeWriteSEPlayer (0);
				Vector2 tapPoint = Camera.main.ScreenToWorldPoint (Input.mousePosition);
				Collider2D collition2d = Physics2D.OverlapPoint (tapPoint);
				if (collition2d) {
					RaycastHit2D hitObject = Physics2D.Raycast (tapPoint, -Vector3.forward);
					if (hitObject) {
						//Debug.Log("hit object is " + hitObject.collider.gameObject.name);

						Vector3 Apos = Player.transform.position;
						Vector3 Bpos = hitObject.transform.position;
						float dis = Vector3.Distance (Apos, Bpos) * 1f;

						if (hitObject.transform.tag == "Player") {
							gtc.GameCrear ();
							isFind = true;
						} else {
							SEPlayer.Instance.SparkSEPlayer ();
							ClickLife--;
						}

						gtc._DistText.text = dis.ToString("f1");
						gtc._LifeText.text = ClickLife.ToString ();

					}
				}
			}
		}
	}
}
