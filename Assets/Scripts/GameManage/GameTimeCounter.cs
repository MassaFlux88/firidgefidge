﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameTimeCounter : MonoBehaviour {

	/// <summary>暗転用黒テクスチャ</summary>
	public Image _HideTexture;
	/// <summary>フェード中の透明度</summary>
	private float fadeAlpha =0;
	/// <summary>フェード中かどうか</summary>
	private bool isFading = false;

	private DummyMaker Dum;
	private CountDown cDown;
	private MouseClick mClick;

	public bool isHideTime; 
	public bool isFindTime;

	private Text _TimeText;
	public Text _DistText;
	public Text _LifeText;

	public int HideTime;
	public int FindTime;

	void Start () {
		isFindTime = false;
		fadeAlpha = 0;

		cDown = this.gameObject.GetComponent<CountDown> ();
		Dum = this.gameObject.GetComponent<DummyMaker> ();
		mClick = this.gameObject.GetComponent<MouseClick> ();
		_TimeText = GameObject.Find ("MainCanvas/TimeText").GetComponent<Text>();
		_DistText = GameObject.Find ("MainCanvas/DistText").GetComponent<Text> ();
		_LifeText = GameObject.Find ("MainCanvas/LifeText").GetComponent<Text> ();
		_HideTexture = GameObject.Find ("MainCanvas/HideTexture").GetComponent<Image>();
		_TimeText.text = "";
		_DistText.text = "";
		_LifeText.text = mClick.ClickLife.ToString();
		_HideTexture.color = new Color(0, 0, 0, this.fadeAlpha);
	}
		
	void Update(){
		if (isFindTime && mClick.ClickLife <= 0) {
			isFindTime = false;
			this.GameOver ();
		}
	}

	public void StartFindCounter(){
		StartCoroutine (StartFindCounterCoroutine ());
		SEPlayer.Instance.NotifySEPlayer (1);
	}

	IEnumerator StartFindCounterCoroutine(){
		cDown._textCountdown.text = "FIND ME!";
		yield return new WaitForSeconds (1.0f);
		cDown._textCountdown.text="";
		isFindTime = true;

		while (FindTime > 0) {
			if (!isFindTime) {
				yield break;
			}
			SEPlayer.Instance.ButtonSEPlayer(3);
			FindTime--;
			_TimeText.text = FindTime.ToString();

			yield return new WaitForSeconds (1.0f);
		}
		isFindTime = false;

		if (!mClick.isFind) {
			this.GameOver();
			yield break;
		}

	}

	public void GameOver(){
		Dum.ChildDelete ();
		cDown._textCountdown.text = "GameOver!";
		SEPlayer.Instance.NotifySEPlayer (1);
	}

	public void GameCrear(){
		isFindTime = false;
		Dum.ChildDelete ();
		cDown._textCountdown.text = "GameClear!!";
		SEPlayer.Instance.NotifySEPlayer (1);
	}
}
