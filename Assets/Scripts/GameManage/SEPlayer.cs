﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SEPlayer : SingletonMonoBehaviour<SEPlayer> {

	public AudioClip[] ButtonSE;
	public AudioClip[] ItemSE;
	public AudioClip[] SparkDamageSE;
	public AudioClip[] NotificationSE;
	public AudioClip[] TypeWriteSE;
	AudioSource audioSource;

	void Start() {
		audioSource = gameObject.GetComponent<AudioSource> ();
	}
	
	public void ButtonSEPlayer(int i){
		audioSource.PlayOneShot( ButtonSE[i] );
	}

	public void ItemSEPlayer(int i){
		audioSource.PlayOneShot( ItemSE[i] );
	}

	public void NotifySEPlayer(int i){
		audioSource.PlayOneShot( NotificationSE[i] );
	}

	public void TypeWriteSEPlayer(int i){
		audioSource.PlayOneShot( TypeWriteSE[i] );
	}

	public void SparkSEPlayer(){
		int i = Random.Range (0, SparkDamageSE.Length);
		audioSource.PlayOneShot( SparkDamageSE[i] );
	}
}
