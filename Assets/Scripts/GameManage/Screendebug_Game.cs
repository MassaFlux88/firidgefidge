﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UniRx;

public class Screendebug_Game : MonoBehaviour {

	PlayerControl playerCtrl;
	public GameObject Player;

	public Text ScoreText;
	public Text DistanceText;
	public Slider FuelGauge;
	public Slider HPGauge;

	void Start () {
		playerCtrl = GameObject.Find("MAPMAKER").GetComponent<MapMaker> ().WashingMachine.GetComponent<PlayerControl>();
	}

	void Update () {
		ScoreText.text = "Score:" + playerCtrl.Score.ToString ();
		DistanceText.text ="Dist:" + playerCtrl.Dist.ToString ("f1") + "m";
		FuelGauge.value = playerCtrl.Fuel / playerCtrl.maxFuel;
		HPGauge.value = playerCtrl.HP / playerCtrl.maxHP;
	}
}
