﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundMaker :MonoBehaviour
{
	public GameObject groundPrefab;

	public GameObject Camera;

	private float Offset;

	private float maxX;
	private float presentX;

	void Start ()
	{
		Camera = GameObject.Find("Main Camera");
		presentX = this.gameObject.transform.position.x;
		Offset = groundPrefab.gameObject.GetComponent<SpriteRenderer> ().bounds.size.x;
	}

	void Update(){
		maxX = Camera.transform.position.x + Screen.width / 16f;

		for (; presentX <= maxX; presentX += Offset) {
			GameObject ground = Instantiate (groundPrefab, new Vector3 (presentX, 0.5f, 0), Quaternion.identity) as GameObject;
			ground.transform.parent = this.transform;

		}

	}
}