﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapMaker : MonoBehaviour {

	private GameObject ITEM;

	public GameObject WashingMachine;
	private int CallNum;

	public GameObject[] groundPrefabs;
	public GameObject[] Clothes;
	public GameObject[] Items;
	public GameObject ChargeBase;

	private GameObject charge;
	private GameObject ground;
	private GameObject cloth;
	private GameObject item;

	private GameObject Camera;
	private PlayerControl playerctrl;

	private GameObject ItemPoint;
	private GameObject ItemPoint2;

	private float Offset;

	private float maxX;
	private float PresentBrickX;

	private float maxItemX;
	private float PresentItemX;

	private bool isFuel;
	public bool isSetBase;


	void Start ()
	{
		CallNum = PlayerPrefs.GetInt ("MachineCall");
		WashingMachine = Instantiate (DataManager.Instance.WashingMachines [CallNum], new Vector3 (0, 7f, 0), Quaternion.identity) as GameObject;

		playerctrl = WashingMachine.GetComponent<PlayerControl>();
		ITEM = GameObject.Find ("ITEMS");
		Camera = GameObject.Find("Main Camera");
		PresentBrickX = this.gameObject.transform.position.x + 6f;
		isFuel = false;
	}

	void Update()
	{
		maxX = Camera.transform.position.x + Screen.width / 32f;
		var Xnum = Random.Range (7, 14);

		if ((int)playerctrl.Fuel <= (int)playerctrl.maxFuel / 2) {
			isFuel = true;
		}

		for (; PresentBrickX <= maxX; PresentBrickX += 1f + Xnum) {
			var num = Random.Range (0, groundPrefabs.Length);
			var Ynum = Random.Range (1,10);

			if ((int)playerctrl.Fuel <= (int)playerctrl.maxFuel / 2) {
				isFuel = true;
			}

			if (isFuel && isSetBase) {
				charge = Instantiate (ChargeBase, new Vector3 (PresentBrickX, 1, 0), Quaternion.identity) as GameObject;
				charge.transform.parent = this.transform;
				isFuel = false;
				isSetBase = false;
			} else {

				ground = Instantiate (groundPrefabs [num], new Vector3 (PresentBrickX, Ynum, 0), Quaternion.identity) as GameObject;
				ground.transform.parent = this.transform;

				PresentItemX = ground.gameObject.transform.position.x;
				maxItemX = PresentItemX + Random.Range (2, 7);

				var ItemMake = Random.Range (1, 101);
				if (ItemMake % 2 == 0) {
					for (; PresentItemX <= maxItemX; PresentItemX += 1.5f) {
						var ItemNum = Random.Range (0, Clothes.Length);

						cloth = Instantiate (Clothes [ItemNum], new Vector3 (PresentItemX, Ynum + 4, 0), Quaternion.identity) as GameObject;
						cloth.transform.parent = ITEM.transform;
					}

				}
				if (ItemMake % 11 == 0) {
					var ItemNum = Random.Range (0, Items.Length);
					item = Instantiate (Items [ItemNum], new Vector3 (PresentItemX, Ynum + 4, 0), Quaternion.identity) as GameObject;
					item.transform.parent = ITEM.transform;
				}
			}
		}
	}

	void BrickMaker(){

	}

	void ItemMaker(){

	}

	void ClothMaker(){

	}
}
