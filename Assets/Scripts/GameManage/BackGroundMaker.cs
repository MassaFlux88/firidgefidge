﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackGroundMaker :MonoBehaviour
{
	public GameObject BackGroundPrefab;
	private GameObject BackGround;

	public Transform Camera;

	private float Offset;
	private float maxBackX;
	private float presentBackX;

	public float rag;
	private float textureOffset;

	void Start ()
	{
		//Camera = GameObject.Find("Main Camera");
		presentBackX = this.gameObject.transform.position.x;
		Offset = BackGroundPrefab.gameObject.GetComponent<SpriteRenderer> ().bounds.size.x;
		rag = 2f;
	}

	void Update(){
		maxBackX = Offset * 2f + Camera.transform.position.x - textureOffset;

		textureOffset = Camera.transform.position.x / rag;
		this.transform.localPosition = new Vector3 (textureOffset ,0 , 0);

		textureOffset = Camera.transform.position.x / rag;

		for (; presentBackX <= maxBackX; presentBackX += Offset) {
			BackGround = Instantiate (BackGroundPrefab, new Vector3 (presentBackX + textureOffset, Camera.transform.position.y, 0), Quaternion.identity) as GameObject;
			BackGround.AddComponent<Background> ();
			BackGround.transform.parent = this.transform;
		}
	}
}