﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStarter : MonoBehaviour {

	public GameObject WashingMachine;
	private int CallNum;

	private MouseClick _mclick;

	void Start ()
	{
		_mclick = gameObject.GetComponent<MouseClick> ();
		//CallNum = PlayerPrefs.GetInt ("MachineCall");
		CallNum = Random.Range(0, DataManager.Instance.WashingMachines.Length);
		/*WashingMachine = Instantiate (DataManager.Instance.WashingMachines [CallNum], new Vector3 (0, 0, 0), Quaternion.identity) as GameObject;
		WashingMachine.AddComponent<PlayerControler> ();
		WashingMachine.tag = "Player";
		_mclick.Player = WashingMachine;*/
		PlayerPrefs.SetInt ("MachineCall", CallNum);
		PlayerPrefs.Save ();
	}
}
