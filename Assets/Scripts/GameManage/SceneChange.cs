﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneChange : SingletonMonoBehaviour<SceneChange> {

	public void OnTitle()
	{
		FadeManager.Instance.LoadLevel("Title",0.25f);
	}

	public void OnBriefing () 
	{
		SEPlayer.Instance.ButtonSEPlayer(1);
		PlayerPrefs.Save ();
		FadeManager.Instance.LoadLevel("Briefing",0.25f);
	}

	public void OnGame()
	{
		SEPlayer.Instance.ButtonSEPlayer(1);
		PlayerPrefs.Save ();
		FadeManager.Instance.LoadLevel("Game",0.25f);
	}

	public void OnRetry() 
	{
		SEPlayer.Instance.ButtonSEPlayer(1);
		PlayerPrefs.Save ();
		FadeManager.Instance.LoadLevel("Game",0.25f);
	}

	public void OnArchives()
	{
		SEPlayer.Instance.ButtonSEPlayer(1);
		PlayerPrefs.Save ();
		FadeManager.Instance.LoadLevel("Archives",0.25f);
	}
}
