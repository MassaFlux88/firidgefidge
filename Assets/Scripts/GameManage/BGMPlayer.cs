﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGMPlayer : SingletonMonoBehaviour<BGMPlayer>{

	public AudioClip[] TitleBGM;
	public AudioClip[] GameBGM;
	AudioSource audioSource;

	public void Awake ()
	{
		if (this != Instance) {
			Destroy (this);
			return;
		}
		DontDestroyOnLoad (this.gameObject);
	}

	void Start ()
	{
		audioSource = gameObject.GetComponent<AudioSource> ();

	}
	
	void Update ()
	{
		if (!audioSource.isPlaying)
		{
			int gamenum = Random.Range (0, GameBGM.Length);
			audioSource.clip = GameBGM [gamenum];
			audioSource.Play();
		}

		if (Application.loadedLevelName == "Title")
		{
			if (!audioSource.isPlaying)
			{
				audioSource.Stop();
				int titlenum = Random.Range (0, TitleBGM.Length);
				audioSource.clip = TitleBGM [titlenum];
				audioSource.Play();
			}
		}
	}
}
