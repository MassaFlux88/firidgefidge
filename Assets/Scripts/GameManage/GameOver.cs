﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class GameOver : SingletonMonoBehaviour<GameOver> {

	[SerializeField]
	private Transform GameOverCanvas;

	[SerializeField]
	private GameObject ButtonGroupe;

	[SerializeField]
	private Text DistText;

	[SerializeField]
	private Text ScoreText;

	[SerializeField]
	private Text TotalText;

	[SerializeField]
	private Text BestText;

	[SerializeField]
	private Text NewRecordText;

	[SerializeField]
	private Button RetryButton;

	[SerializeField]
	private Button QuitButton;

	[SerializeField]
	private Button ShareButton;

	private float Total;
	private float Dist;
	private float BestScore;

	string text;
	string URL;

	void Start () {
		GameOverCanvas.gameObject.SetActive (false);
		ButtonGroupe.gameObject.SetActive (false);
	}

	public void GameOverMessage(int Score, float distance){
		StartCoroutine(this.GameOverPopUp (Score, distance));
		Dist = distance;
	}
		
	IEnumerator GameOverPopUp(int Score, float distance)
	{
		yield return new WaitForSeconds (1.0f);

		GameOverCanvas.gameObject.SetActive (true);
		yield return new WaitForSeconds (1.0f);

		Total = Score + (distance * 10f);
		BestScore = PlayerPrefs.GetFloat ("BestScore");

		ScoreText.text = "Score\t: " + Score.ToString();
		yield return new WaitForSeconds (0.5f);

		DistText.text = "Dist\t\t: " + Dist.ToString ("f1") + "M x10";
		yield return new WaitForSeconds (0.5f);

		TotalText.text = "Total\t: " + Total.ToString ("f0") + "Points";

		if (Total > BestScore) {
			BestScore = Total;
			PlayerPrefs.SetFloat ("BestScore", BestScore);
			PlayerPrefs.Save();
			NewRecordText.text = "New Record!!";
		}

		BestText.text = "Best \t: " + BestScore.ToString("f0") + "Points";

		yield return new WaitForSeconds (0.5f);

		ButtonGroupe.gameObject.SetActive (true);
	}

	public void OnShare(){
		//string tweet ="結果投稿 テスト\t" + "スコア: " + Total.ToString("f0") + "点";
		//Application.OpenURL("https://twitter.com/intent/tweet?text="+ WWW.EscapeURL(tweet));
		CaptureAndShareGameOver ();
	}

	/*public void GameOverShare ()
	{
		string msg = "ゲームオーバー！" + Total.ToString("f0") + "点獲得\n"+ Dist.ToString("f1") + "m飛行した。";
		StartCoroutine (GameOverShare (msg));
	}

	public IEnumerator GameOverShare(string msg){

		string fileName = "WashBash" + System.DateTime.Now.ToString ("yyyyMMddHHmmss") + ".png";
		Application.CaptureScreenshot (fileName);

		yield return new WaitForEndOfFrame ();

		if (Application.platform == RuntimePlatform.Android) {
			text = "";
			URL = "";

		} else if (Application.platform == RuntimePlatform.IPhonePlayer) {
			text = "";
			URL = "";
		}
		yield return new WaitForSeconds (1);
		string imgPath = Application.persistentDataPath + "/" + fileName;

		SocialConnector.SocialConnector.Share (text, URL, imgPath);
	}*/

	public void CaptureAndShareGameOver ()
	{
		string msg = "ゲームオーバー！" + Total.ToString("f0") + "点獲得\n"+ Dist.ToString("f1") + "m飛行した。";
		StartCoroutine (CaptureAndShareEnumerator (msg));
	}

	private IEnumerator CaptureAndShareEnumerator (string msg)
	{
		string fileName = "WashBash" + System.DateTime.Now.ToString ("yyyyMMddHHmmss") + ".png";
		Application.CaptureScreenshot (fileName);

		yield return new WaitForSeconds (0.5f);

		#if UNITY_EDITOR
		Debug.Log (msg);
		#elif UNITY_ANDROID || UNITY_IOS
		string imgPath = Application.persistentDataPath + "/" + fileName;
		SocialConnector.SocialConnector.Share (msg, null, imgPath);
		#endif
	}

}
