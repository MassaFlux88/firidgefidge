﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;

public class CountDown : MonoBehaviour
{
	public Text _textCountdown;
	private Button _StartButton;
	private Canvas _StartCanvas;
	private Image _SkeltonImage;

	private GameTimeCounter gtc;

	void Start()
	{
		_StartCanvas = GameObject.Find ("StartCanvas").GetComponent<Canvas> ();
		_StartButton = GameObject.Find ("StartCanvas/StartButton").GetComponent<Button> ();
		_textCountdown = GameObject.Find ("StartCanvas/CountDownText").GetComponent<Text> ();
		//_SkeltonImage = GameObject.Find ("StartCanvas/SkeltonImage").GetComponent<Image> ();
		_textCountdown.text = "";

		gtc = this.gameObject.GetComponent<GameTimeCounter>();
	}

	public void OnClickButtonStart()
	{
		StartCoroutine(StartCountdownCoroutine());
	}

	IEnumerator StartCountdownCoroutine()
	{
		_textCountdown.gameObject.SetActive(true);
		_StartButton.gameObject.SetActive(false);

		_textCountdown.text = "3";
		SEPlayer.Instance.ButtonSEPlayer(3);
		yield return new WaitForSeconds(1.0f);

		_textCountdown.text = "2";
		SEPlayer.Instance.ButtonSEPlayer(3);
		yield return new WaitForSeconds(1.0f);

		_textCountdown.text = "1";
		SEPlayer.Instance.ButtonSEPlayer(3);
		yield return new WaitForSeconds(1.0f);

		_textCountdown.text = "HIDE!";
		SEPlayer.Instance.NotifySEPlayer (1);

		gtc.StartFindCounter();
		yield return new WaitForSeconds(1.0f);

		_textCountdown.text = "";

	}
}