﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSelect : MonoBehaviour {

	private int CallNumber;

	public Transform CenterOfMachine;
	private GameObject WashingMachine;

	public Text MachineName;
	public GameObject MachineImg;

	void Start(){
		CallNumber = 0;
		this.DisplayMachine (CallNumber);

	}

	public void OnNext(){
		CallNumber++;
		if (CallNumber > DataManager.Instance.WashingMachines.Length - 1) {
			CallNumber = 0;
		}

		SEPlayer.Instance.ButtonSEPlayer (1);

		this.DisplayMachine (CallNumber);
	}

	public void OnPrevious(){
		CallNumber--;
		if (CallNumber < 0) {
			CallNumber = DataManager.Instance.WashingMachines.Length - 1;
		}

		SEPlayer.Instance.ButtonSEPlayer (1);

		this.DisplayMachine (CallNumber);
	}

	void DisplayMachine(int Callnum){
		WashingMachine = DataManager.Instance.WashingMachines [Callnum];
		MachineName.text = DataManager.Instance.WashingMachines [Callnum].name;
		MachineImg.GetComponent<SpriteRenderer> ().sprite = DataManager.Instance.WashingMachines [Callnum].GetComponent<SpriteRenderer> ().sprite;

		DataManager.Instance.MachineCallNum = Callnum;
		PlayerPrefs.SetInt("MachineCall",DataManager.Instance.MachineCallNum);
		}

}
