﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyMaker : MonoBehaviour {
	
	private GameObject WashingMachine;
	private GameObject _Parent;

	private MouseClick _mclick;

	public int _numberOfDummy;

	private int CallNum;

	void Start () {
		_Parent = GameObject.Find ("DummyParent");
		_mclick = gameObject.GetComponent<MouseClick> ();
		this.DummyMake ();
	}
	
	public void DummyMake(){
		CallNum = PlayerPrefs.GetInt ("MachineCall");

		WashingMachine = Instantiate (DataManager.Instance.WashingMachines [CallNum]) as GameObject;
		WashingMachine.transform.localPosition = new Vector2(Random.Range(-2.5f,2.5f),Random.Range(-3f,4.5f));
		WashingMachine.tag = "Player";
		_mclick.Player = WashingMachine;

		for (int i = 0; i < _numberOfDummy; i++) {

			WashingMachine = Instantiate (DataManager.Instance.WashingMachines [CallNum]) as GameObject;
			WashingMachine.transform.localPosition = new Vector2(Random.Range(-2.5f,2.5f),Random.Range(-3f,4.5f));
			WashingMachine.tag = "Dummy";
			WashingMachine.GetComponent<BoxCollider2D> ().isTrigger = true;
			WashingMachine.transform.parent = _Parent.transform;
		}
	}

	public void ChildDelete(){
		foreach ( Transform n in _Parent.transform )
		{
			GameObject.Destroy(n.gameObject);
		}
	}
}
