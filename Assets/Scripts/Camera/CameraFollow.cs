﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {
	public GameObject target;

	public float max;

	void Start () {
		target = GameObject.FindWithTag("Player");

		max = 0f;
		transform.position = new Vector3 (0, 7, -1);
	}

	void LateUpdate () {
		if (target.transform.position.x >= max) {
			max = target.transform.position.x;
		}
		if (target.transform.position.x >= max) {
			transform.position = new Vector3 (target.transform.position.x, 7, -1);
		}
	}
}